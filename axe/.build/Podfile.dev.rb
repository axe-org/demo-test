source "https://github.com/axe-org/demo-private-spec.git"
#source "https://github.com/CocoaPods/Specs.git"

pod 'Ground/release'

target "Test" do
  # 标记插入地址。
  
  pod 'Login' , '>= 0.0.2-beta.0', '< 1.0.0'

end

target "Localhost" do
  # Pods for Test
  pod "Test/source", :path=> "./"
  pod "Login/release"
end

# 处理 同名 scheme问题。
post_install do |installer|
  installer.pods_project.targets.each do |target|
    # 设置为模块打包scheme名， 以避免pod创建同名的scheme
    if target.name.eql?("Test")
        target.name = "LocalTest"
    end
  end
end
